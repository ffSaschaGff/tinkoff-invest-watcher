-- +goose Up 
-- +goose NO TRANSACTION
ALTER TABLE figi
  ADD COLUMN class_code TEXT,
  ADD COLUMN exchange TEXT,
  ADD COLUMN real_exchange TEXT;
-- +goose Down 
-- +goose NO TRANSACTION
ALTER TABLE figi
  DROP COLUMN class_code,
  DROP COLUMN exchange,
  DROP COLUMN real_exchange;
