package marketdata_test

import (
	"database/sql"
	"errors"
	"log"
	"math"
	"math/rand"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/ssummers02/invest-api-go-sdk/pkg/investapi"
	"github.com/stretchr/testify/require"
	"gitlab.com/ffSaschaGff/tinkoff-invest-watcher/internal/marketdata"
	"gitlab.com/ffSaschaGff/tinkoff-invest-watcher/internal/mock_api"
	"gitlab.com/ffSaschaGff/tinkoff-invest-watcher/internal/optional"
	"gitlab.com/ffSaschaGff/tinkoff-invest-watcher/internal/repository"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type appFields struct {
	tAPI  marketdata.TinkoffAPI
	repo  marketdata.MarketRepo
	timer marketdata.Timer
}

var errSome = errors.New("some error")

func TestInitMarketData(t *testing.T) {
	t.Parallel()

	type testInit struct {
		name        string
		fields      appFields
		expectedErr error
	}

	gazp := shareGAZP()
	gazpInstrument := shareToInstrument(gazp)
	gazpInstrument.TriggerLevel = 10
	sber := shareSBER()
	sberInstrument := shareToInstrument(sber)
	apiShares := []*investapi.Share{gazp, sber}

	tests := []testInit{
		{
			name: "tinkoff api error",
			fields: appFields{
				tAPI: func() marketdata.TinkoffAPI {
					mock := mock_api.NewMockTinkoffAPI(gomock.NewController(t))
					mock.EXPECT().
						Shares(investapi.InstrumentStatus_INSTRUMENT_STATUS_ALL).
						Return(nil, errSome)
					return mock
				}(),
				repo: func() marketdata.MarketRepo {
					mock := mock_api.NewMockMarketRepo(gomock.NewController(t))
					return mock
				}(),
			},
			expectedErr: errSome,
		},
		{
			name: "valid case",
			fields: appFields{
				tAPI: func() marketdata.TinkoffAPI {
					mock := mock_api.NewMockTinkoffAPI(gomock.NewController(t))
					mock.EXPECT().
						Shares(investapi.InstrumentStatus_INSTRUMENT_STATUS_ALL).
						Return(apiShares, nil)
					return mock
				}(),
				repo: func() marketdata.MarketRepo {
					mock := mock_api.NewMockMarketRepo(gomock.NewController(t))
					mock.EXPECT().
						ReadInstrument(repository.Instrument{
							Figi:         gazp.Figi,
							ClassCode:    sql.NullString{Valid: true, String: ""},
							Exchange:     sql.NullString{Valid: true, String: ""},
							RealExchange: sql.NullString{Valid: true, String: ""},
						}).
						Return(gazpInstrument, nil)
					mock.EXPECT().
						WriteInstrument(repository.Instrument{
							Figi:         gazp.Figi,
							Name:         gazp.Name,
							Tiker:        gazp.Ticker,
							Currency:     gazp.Currency,
							MinPrice:     gazp.MinPriceIncrement.Nano,
							Lot:          gazp.Lot,
							TriggerLevel: gazpInstrument.TriggerLevel,
							Active:       gazpInstrument.Active,
							ClassCode:    sql.NullString{Valid: true, String: "TQBR"},
							Exchange:     sql.NullString{Valid: true, String: "MOEX_EVENING_WEEKEND"},
							RealExchange: sql.NullString{Valid: true, String: "REAL_EXCHANGE_MOEX"},
						}).
						Return(nil)
					mock.EXPECT().
						ReadInstrument(repository.Instrument{
							Figi:         sber.Figi,
							ClassCode:    sql.NullString{Valid: true, String: ""},
							Exchange:     sql.NullString{Valid: true, String: ""},
							RealExchange: sql.NullString{Valid: true, String: ""},
						}).
						Return(sberInstrument, sql.ErrNoRows)
					mock.EXPECT().
						WriteInstrument(repository.Instrument{
							Figi:         sber.Figi,
							Name:         sber.Name,
							Tiker:        sber.Ticker,
							Currency:     sber.Currency,
							MinPrice:     sber.MinPriceIncrement.Nano,
							Lot:          sber.Lot,
							TriggerLevel: 7,
							Active:       false,
							ClassCode:    sql.NullString{Valid: true, String: "TQBR"},
							Exchange:     sql.NullString{Valid: true, String: "MOEX_EVENING_WEEKEND"},
							RealExchange: sql.NullString{Valid: true, String: "REAL_EXCHANGE_MOEX"},
						}).
						Return(nil)
					return mock
				}(),
			},
			expectedErr: nil,
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			app := marketdata.NewApp(test.fields.tAPI, test.fields.repo, test.fields.timer)
			err := app.InitMarketData()
			if test.expectedErr == nil {
				require.Nil(t, err)
			} else {
				require.ErrorIs(t, err, test.expectedErr)
			}
		})
	}
}

func TestLoadPrices(t *testing.T) {
	t.Parallel()

	type testLoadPrices struct {
		name   string
		fields appFields
	}

	gazp := instrumentGAZP()
	sber := instrumentSBER()
	apiShares := []repository.Instrument{gazp, sber}
	bigPackSize := 100
	apiSharesBigPack := make([]repository.Instrument, 0, bigPackSize)
	for i := 0; i < bigPackSize; i++ {
		apiSharesBigPack = append(apiSharesBigPack, gazp)
	}
	pricesSber := instrumentPrices()
	pricesGazp := instrumentPrices()
	eventBell := repository.EventBell{
		Time:       pricesSber[1].Time.Seconds,
		Price:      120.23,
		Instrument: sber,
	}
	eventVolume := repository.EventBigVolume{
		Time:       pricesGazp[1].Time.Seconds,
		Instrument: gazp,
	}
	now := time.Now()
	/*eventChange := repository.EventBigChange{
		Time:       pricesSber[1].Time.Seconds,
		EventType:  int(marketdata.Down15),
		Instrument: sber,
	}*/

	tests := []testLoadPrices{
		{
			name: "valid load big pack",
			fields: appFields{
				tAPI: func() marketdata.TinkoffAPI {
					m := mock_api.NewMockTinkoffAPI(gomock.NewController(t))
					m.EXPECT().
						GetCandles(gazp.Figi,
							gomock.Any(),
							gomock.Any(),
							investapi.CandleInterval_CANDLE_INTERVAL_DAY).
						Return(pricesGazp, nil).Times(bigPackSize)
					return m
				}(),
				repo: func() marketdata.MarketRepo {
					m := mock_api.NewMockMarketRepo(gomock.NewController(t))
					m.EXPECT().
						GetActiveInstruments().
						Return(apiSharesBigPack, nil).Times(2)
					m.EXPECT().WriteCandle(repository.Candle{
						Figi:   gazp.Figi,
						Time:   pricesGazp[0].Time.Seconds,
						Open:   float64(pricesGazp[0].Open.Units),
						Close:  float64(pricesGazp[0].Close.Units),
						Low:    float64(pricesGazp[0].Low.Units),
						High:   float64(pricesGazp[0].High.Units),
						Volume: pricesGazp[0].Volume,
					}).Return(nil).Times(bigPackSize)
					m.EXPECT().
						WriteCandle(repository.Candle{
							Figi:   gazp.Figi,
							Time:   pricesGazp[1].Time.Seconds,
							Open:   float64(pricesGazp[1].Open.Units),
							Close:  float64(pricesGazp[1].Close.Units),
							Low:    float64(pricesGazp[1].Low.Units),
							High:   float64(pricesGazp[1].High.Units),
							Volume: pricesGazp[1].Volume,
						}).
						Return(nil).Times(bigPackSize)
					m.EXPECT().NewEventsBell().Return([]repository.EventBell{})
					m.EXPECT().ListCandles(gomock.Any()).Return(nil, nil)
					m.EXPECT().ListEvents(gomock.Any()).Return(nil, nil)
					m.EXPECT().NewEventsBigVolume().Return([]repository.EventBigVolume{})
					return m
				}(),
				timer: func() marketdata.Timer {
					m := mock_api.NewMockTimer(gomock.NewController(t))
					m.EXPECT().Now().Return(now).AnyTimes()
					return m
				}(),
			},
		},
		{
			name: "valid load",
			fields: appFields{
				tAPI: func() marketdata.TinkoffAPI {
					m := mock_api.NewMockTinkoffAPI(gomock.NewController(t))
					m.EXPECT().
						GetCandles(gazp.Figi,
							gomock.Any(),
							gomock.Any(),
							investapi.CandleInterval_CANDLE_INTERVAL_DAY).
						Return(pricesGazp, nil)
					m.EXPECT().
						GetCandles(sber.Figi,
							gomock.Any(),
							gomock.Any(),
							investapi.CandleInterval_CANDLE_INTERVAL_DAY).
						Return(pricesSber, nil)
					return m
				}(),
				repo: func() marketdata.MarketRepo {
					m := mock_api.NewMockMarketRepo(gomock.NewController(t))
					m.EXPECT().
						GetActiveInstruments().
						Return(apiShares, nil).Times(2)
					m.EXPECT().WriteCandle(repository.Candle{
						Figi:   sber.Figi,
						Time:   pricesSber[0].Time.Seconds,
						Open:   float64(pricesSber[0].Open.Units),
						Close:  float64(pricesSber[0].Close.Units),
						Low:    float64(pricesSber[0].Low.Units),
						High:   float64(pricesSber[0].High.Units),
						Volume: pricesSber[0].Volume,
					}).Return(nil)
					m.EXPECT().WriteCandle(repository.Candle{
						Figi:   sber.Figi,
						Time:   pricesSber[1].Time.Seconds,
						Open:   float64(pricesSber[1].Open.Units),
						Close:  float64(pricesSber[1].Close.Units),
						Low:    float64(pricesSber[1].Low.Units),
						High:   float64(pricesSber[1].High.Units),
						Volume: pricesSber[1].Volume,
					}).Return(nil)
					m.EXPECT().WriteCandle(repository.Candle{
						Figi:   gazp.Figi,
						Time:   pricesGazp[0].Time.Seconds,
						Open:   float64(pricesGazp[0].Open.Units),
						Close:  float64(pricesGazp[0].Close.Units),
						Low:    float64(pricesGazp[0].Low.Units),
						High:   float64(pricesGazp[0].High.Units),
						Volume: pricesGazp[0].Volume,
					}).Return(nil)
					m.EXPECT().
						WriteCandle(repository.Candle{
							Figi:   gazp.Figi,
							Time:   pricesGazp[1].Time.Seconds,
							Open:   float64(pricesGazp[1].Open.Units),
							Close:  float64(pricesGazp[1].Close.Units),
							Low:    float64(pricesGazp[1].Low.Units),
							High:   float64(pricesGazp[1].High.Units),
							Volume: pricesGazp[1].Volume,
						}).
						Return(nil)
					m.EXPECT().NewEventsBell().Return([]repository.EventBell{eventBell})
					m.EXPECT().ListCandles(repository.ListCandlesFilter{
						From: now.Add(-24 * 5 * time.Hour),
						Figis: optional.From([]string{
							gazp.Figi, sber.Figi,
						}),
					}).Return(nil, nil)
					m.EXPECT().ListEvents(repository.ListEventsFilter{
						From: now.Add(-24 * 5 * time.Hour),
						Figis: optional.From([]string{
							gazp.Figi, sber.Figi,
						}),
					}).Return(nil, nil)
					m.EXPECT().NewEventsBigVolume().Return([]repository.EventBigVolume{eventVolume})
					m.EXPECT().
						WriteMessage(repository.Message{
							Figi:    sber.Figi,
							Time:    eventBell.Time,
							MsgType: 0,
							Message: "Цена SBER (Сбербанк) достигла 120.23",
						}).
						Return(nil)
					m.EXPECT().
						WriteMessage(repository.Message{
							Figi:    gazp.Figi,
							Time:    eventVolume.Time,
							MsgType: int(marketdata.Volume),
							Message: "Оборот по GAZP (Газпром) превысил m+3σ",
						}).
						Return(nil)
					/*m.EXPECT().
					WriteMessage(repository.Message{
						Figi:    sber.Figi,
						Time:    eventChange.Time,
						MsgType: int(marketdata.Down15),
						Message: "SBER (Сбербанк) упал на 14%",
					}).
					Return(nil)*/
					return m
				}(),
				timer: func() marketdata.Timer {
					m := mock_api.NewMockTimer(gomock.NewController(t))
					m.EXPECT().Now().Return(now).AnyTimes()
					return m
				}(),
			},
		},
		{
			name: "load prices: on get message error",
			fields: appFields{
				tAPI: func() marketdata.TinkoffAPI {
					m := mock_api.NewMockTinkoffAPI(gomock.NewController(t))
					m.EXPECT().
						GetCandles(gazp.Figi,
							gomock.Any(),
							gomock.Any(),
							investapi.CandleInterval_CANDLE_INTERVAL_DAY).
						Return(pricesGazp, nil)
					m.EXPECT().
						GetCandles(sber.Figi,
							gomock.Any(),
							gomock.Any(),
							investapi.CandleInterval_CANDLE_INTERVAL_DAY).
						Return(pricesSber, nil)
					return m
				}(),
				repo: func() marketdata.MarketRepo {
					m := mock_api.NewMockMarketRepo(gomock.NewController(t))
					m.EXPECT().
						GetActiveInstruments().
						Return(apiShares, nil).Times(2)
					m.EXPECT().WriteCandle(repository.Candle{
						Figi:   sber.Figi,
						Time:   pricesSber[0].Time.Seconds,
						Open:   float64(pricesSber[0].Open.Units),
						Close:  float64(pricesSber[0].Close.Units),
						Low:    float64(pricesSber[0].Low.Units),
						High:   float64(pricesSber[0].High.Units),
						Volume: pricesSber[0].Volume,
					}).Return(nil)
					m.EXPECT().WriteCandle(repository.Candle{
						Figi:   sber.Figi,
						Time:   pricesSber[1].Time.Seconds,
						Open:   float64(pricesSber[1].Open.Units),
						Close:  float64(pricesSber[1].Close.Units),
						Low:    float64(pricesSber[1].Low.Units),
						High:   float64(pricesSber[1].High.Units),
						Volume: pricesSber[1].Volume,
					}).Return(nil)
					m.EXPECT().WriteCandle(repository.Candle{
						Figi:   gazp.Figi,
						Time:   pricesGazp[0].Time.Seconds,
						Open:   float64(pricesGazp[0].Open.Units),
						Close:  float64(pricesGazp[0].Close.Units),
						Low:    float64(pricesGazp[0].Low.Units),
						High:   float64(pricesGazp[0].High.Units),
						Volume: pricesGazp[0].Volume,
					}).Return(nil)
					m.EXPECT().
						WriteCandle(repository.Candle{
							Figi:   gazp.Figi,
							Time:   pricesGazp[1].Time.Seconds,
							Open:   float64(pricesGazp[1].Open.Units),
							Close:  float64(pricesGazp[1].Close.Units),
							Low:    float64(pricesGazp[1].Low.Units),
							High:   float64(pricesGazp[1].High.Units),
							Volume: pricesGazp[1].Volume,
						}).
						Return(nil)
					m.EXPECT().NewEventsBell().Return([]repository.EventBell{eventBell})
					m.EXPECT().ListCandles(repository.ListCandlesFilter{
						From: now.Add(-24 * 5 * time.Hour),
						Figis: optional.From([]string{
							gazp.Figi, sber.Figi,
						}),
					}).Return(nil, nil)
					m.EXPECT().ListEvents(repository.ListEventsFilter{
						From: now.Add(-24 * 5 * time.Hour),
						Figis: optional.From([]string{
							gazp.Figi, sber.Figi,
						}),
					}).Return(nil, nil)
					m.EXPECT().NewEventsBigVolume().Return([]repository.EventBigVolume{eventVolume})
					m.EXPECT().
						WriteMessage(repository.Message{
							Figi:    sber.Figi,
							Time:    eventBell.Time,
							MsgType: 0,
							Message: "Цена SBER (Сбербанк) достигла 120.23",
						}).
						Return(nil)
					m.EXPECT().
						WriteMessage(repository.Message{
							Figi:    gazp.Figi,
							Time:    eventVolume.Time,
							MsgType: int(marketdata.Volume),
							Message: "Оборот по GAZP (Газпром) превысил m+3σ",
						}).
						Return(nil)
					/*m.EXPECT().
					WriteMessage(repository.Message{
						Figi:    sber.Figi,
						Time:    eventChange.Time,
						MsgType: int(marketdata.Down15),
						Message: "SBER (Сбербанк) упал на 14%",
					}).
					Return(nil)*/
					return m
				}(),
				timer: func() marketdata.Timer {
					m := mock_api.NewMockTimer(gomock.NewController(t))
					m.EXPECT().Now().Return(now).AnyTimes()
					return m
				}(),
			},
		},
		{
			name: "on tinkoff api error",
			fields: appFields{
				tAPI: func() marketdata.TinkoffAPI {
					m := mock_api.NewMockTinkoffAPI(gomock.NewController(t))
					m.EXPECT().
						GetCandles(gazp.Figi,
							gomock.Any(),
							gomock.Any(),
							investapi.CandleInterval_CANDLE_INTERVAL_DAY).
						Return(nil, errSome)
					m.EXPECT().
						GetCandles(sber.Figi,
							gomock.Any(),
							gomock.Any(),
							investapi.CandleInterval_CANDLE_INTERVAL_DAY).
						Return(nil, errSome)
					return m
				}(),
				repo: func() marketdata.MarketRepo {
					m := mock_api.NewMockMarketRepo(gomock.NewController(t))
					m.EXPECT().
						GetActiveInstruments().
						Return(apiShares, nil).Times(2)
					m.EXPECT().NewEventsBell().Return(nil)
					m.EXPECT().ListCandles(repository.ListCandlesFilter{
						From: now.Add(-24 * 5 * time.Hour),
						Figis: optional.From([]string{
							gazp.Figi, sber.Figi,
						}),
					}).Return(nil, nil)
					m.EXPECT().ListEvents(repository.ListEventsFilter{
						From: now.Add(-24 * 5 * time.Hour),
						Figis: optional.From([]string{
							gazp.Figi, sber.Figi,
						}),
					}).Return(nil, nil)
					m.EXPECT().NewEventsBigVolume().Return(nil)
					/*m.EXPECT().
						WriteMessage(repository.Message{
							Figi:    sber.Figi,
							Time:    eventChange.Time,
							MsgType: int(marketdata.Down7),
							Message: "SBER (Сбербанк) упал на 7%",
						}).
						Return(nil)
					m.EXPECT().
						WriteMessage(repository.Message{
							Figi:    sber.Figi,
							Time:    eventChange.Time,
							MsgType: int(marketdata.Down30),
							Message: "SBER (Сбербанк) упал на 28%",
						}).
						Return(nil)*/
					return m
				}(),
				timer: func() marketdata.Timer {
					m := mock_api.NewMockTimer(gomock.NewController(t))
					m.EXPECT().Now().Return(now).AnyTimes()
					return m
				}(),
			},
		},
		{
			name: "on get active error",
			fields: appFields{
				tAPI: func() marketdata.TinkoffAPI {
					m := mock_api.NewMockTinkoffAPI(gomock.NewController(t))
					return m
				}(),
				repo: func() marketdata.MarketRepo {
					m := mock_api.NewMockMarketRepo(gomock.NewController(t))
					m.EXPECT().
						GetActiveInstruments().
						Return(nil, errSome)
					return m
				}(),
				timer: func() marketdata.Timer {
					m := mock_api.NewMockTimer(gomock.NewController(t))
					return m
				}(),
			},
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			app := marketdata.NewApp(test.fields.tAPI, test.fields.repo, test.fields.timer)
			app.LoadPrices()
		})
	}
}

func TestEventBigChangeMessage(t *testing.T) {
	t.Parallel()

	type test struct {
		name           string
		eventBigChange marketdata.EventBigChange
		expMessage     *marketdata.Message
		hasErr         bool
	}

	gazp := instrumentGAZP()
	eventTime := time.Now().Unix()

	tests := []test{
		{
			name: "up 1 level",
			eventBigChange: marketdata.EventBigChange{
				Time:      eventTime,
				EventType: marketdata.Up7,
				Instrument: marketdata.Instrument{
					Figi:         gazp.Figi,
					Name:         gazp.Name,
					Tiker:        gazp.Tiker,
					Currency:     gazp.Currency,
					MinPrice:     gazp.MinPrice,
					Lot:          gazp.Lot,
					TriggerLevel: gazp.TriggerLevel,
					Active:       gazp.Active,
				},
			},
			hasErr: false,
			expMessage: &marketdata.Message{
				Figi:    gazp.Figi,
				Message: "GAZP (Газпром) вырос на 7%",
				Time:    eventTime,
				MsgType: marketdata.Up7,
				Sended:  false,
			},
		},
		{
			name: "up 2 level",
			eventBigChange: marketdata.EventBigChange{
				Time:      eventTime,
				EventType: marketdata.Up15,
				Instrument: marketdata.Instrument{
					Figi:         gazp.Figi,
					Name:         gazp.Name,
					Tiker:        gazp.Tiker,
					Currency:     gazp.Currency,
					MinPrice:     gazp.MinPrice,
					Lot:          gazp.Lot,
					TriggerLevel: gazp.TriggerLevel,
					Active:       gazp.Active,
				},
			},
			hasErr: false,
			expMessage: &marketdata.Message{
				Figi:    gazp.Figi,
				Message: "GAZP (Газпром) вырос на 14%",
				Time:    eventTime,
				MsgType: marketdata.Up15,
				Sended:  false,
			},
		},
		{
			name: "up 4 level",
			eventBigChange: marketdata.EventBigChange{
				Time:      eventTime,
				EventType: marketdata.Up30,
				Instrument: marketdata.Instrument{
					Figi:         gazp.Figi,
					Name:         gazp.Name,
					Tiker:        gazp.Tiker,
					Currency:     gazp.Currency,
					MinPrice:     gazp.MinPrice,
					Lot:          gazp.Lot,
					TriggerLevel: gazp.TriggerLevel,
					Active:       gazp.Active,
				},
			},
			hasErr: false,
			expMessage: &marketdata.Message{
				Figi:    gazp.Figi,
				Message: "GAZP (Газпром) вырос на 28%",
				Time:    eventTime,
				MsgType: marketdata.Up30,
				Sended:  false,
			},
		},
		{
			name: "wrong level",
			eventBigChange: marketdata.EventBigChange{
				Time:      eventTime,
				EventType: 9,
				Instrument: marketdata.Instrument{
					Figi:         gazp.Figi,
					Name:         gazp.Name,
					Tiker:        gazp.Tiker,
					Currency:     gazp.Currency,
					MinPrice:     gazp.MinPrice,
					Lot:          gazp.Lot,
					TriggerLevel: gazp.TriggerLevel,
					Active:       gazp.Active,
				},
			},
			hasErr:     true,
			expMessage: nil,
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			message, err := test.eventBigChange.Message()

			require.Equal(t, message, test.expMessage)
			require.Equal(t, err != nil, test.hasErr)
		})
	}
}

func TestClearGarbage(t *testing.T) {
	t.Parallel()

	type testClear struct {
		name   string
		fields appFields
	}

	tests := []testClear{
		{
			name: "valid clear",
			fields: appFields{
				repo: func() marketdata.MarketRepo {
					m := mock_api.NewMockMarketRepo(gomock.NewController(t))
					m.EXPECT().ClearGarbage()
					return m
				}(),
			},
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			app := marketdata.NewApp(test.fields.tAPI, test.fields.repo, test.fields.timer)
			app.ClearGarbage()
		})
	}
}

func TestUpdateAggregates(t *testing.T) {
	t.Parallel()

	type testUpdate struct {
		name   string
		fields appFields
	}

	tests := []testUpdate{
		{
			name: "valid update",
			fields: appFields{
				repo: func() marketdata.MarketRepo {
					m := mock_api.NewMockMarketRepo(gomock.NewController(t))
					m.EXPECT().UpdateAggregates()
					return m
				}(),
			},
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			app := marketdata.NewApp(test.fields.tAPI, test.fields.repo, test.fields.timer)
			app.UpdateAggregates()
		})
	}
}

func TestGetNewEvents(t *testing.T) {
	t.Parallel()

	type testEvents struct {
		name   string
		fields appFields
	}

	tests := []testEvents{
		{
			name: "valid get events",
			fields: appFields{
				repo: func() marketdata.MarketRepo {
					m := mock_api.NewMockMarketRepo(gomock.NewController(t))
					m.EXPECT().GetMessages()
					return m
				}(),
			},
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			app := marketdata.NewApp(test.fields.tAPI, test.fields.repo, test.fields.timer)
			app.GetNewEvents()
		})
	}
}

func TestSendTikerInfo(t *testing.T) {
	t.Parallel()

	type testInfo struct {
		name        string
		fields      appFields
		tiker       string
		expectedErr error
	}

	gazp := instrumentGAZP()
	timeAsk := time.Now()

	tests := []testInfo{
		{
			name: "valid tiker info",
			fields: appFields{
				repo: func() marketdata.MarketRepo {
					m := mock_api.NewMockMarketRepo(gomock.NewController(t))
					m.EXPECT().
						ReadInstrument(repository.Instrument{
							Tiker:        gazp.Tiker,
							ClassCode:    sql.NullString{Valid: true, String: ""},
							Exchange:     sql.NullString{Valid: true, String: ""},
							RealExchange: sql.NullString{Valid: true, String: ""},
						}).
						Return(gazp, nil)
					m.EXPECT().GetBells(gazp.Figi).Return([]float64{120, 100.45}, nil)
					m.EXPECT().
						WriteMessage(repository.Message{
							Time:    timeAsk.Unix(),
							Message: "GAZP: Газпром отслеживается\nКолокольчики:\n120\n100.45",
						}).
						Return(nil)
					return m
				}(),
				timer: func() marketdata.Timer {
					m := mock_api.NewMockTimer(gomock.NewController(t))
					m.EXPECT().Now().Return(timeAsk)
					return m
				}(),
			},
			tiker:       gazp.Tiker,
			expectedErr: nil,
		},
		{
			name: "valid tiker info, no bells",
			fields: appFields{
				repo: func() marketdata.MarketRepo {
					m := mock_api.NewMockMarketRepo(gomock.NewController(t))
					m.EXPECT().
						ReadInstrument(repository.Instrument{
							Tiker:        gazp.Tiker,
							ClassCode:    sql.NullString{Valid: true, String: ""},
							Exchange:     sql.NullString{Valid: true, String: ""},
							RealExchange: sql.NullString{Valid: true, String: ""},
						}).
						Return(gazp, nil)
					m.EXPECT().GetBells(gazp.Figi).Return([]float64{}, nil)
					m.EXPECT().
						WriteMessage(repository.Message{
							Time:    timeAsk.Unix(),
							Message: "GAZP: Газпром отслеживается\nКолокольчиков нет",
						}).
						Return(nil)
					return m
				}(),
				timer: func() marketdata.Timer {
					m := mock_api.NewMockTimer(gomock.NewController(t))
					m.EXPECT().Now().Return(timeAsk)
					return m
				}(),
			},
			tiker:       gazp.Tiker,
			expectedErr: nil,
		},
		{
			name: "wrong tiker on tiker info",
			fields: appFields{
				repo: func() marketdata.MarketRepo {
					m := mock_api.NewMockMarketRepo(gomock.NewController(t))
					m.EXPECT().
						ReadInstrument(repository.Instrument{
							Tiker:        gazp.Tiker,
							ClassCode:    sql.NullString{Valid: true, String: ""},
							Exchange:     sql.NullString{Valid: true, String: ""},
							RealExchange: sql.NullString{Valid: true, String: ""},
						}).
						Return(repository.Instrument{}, sql.ErrNoRows)
					m.EXPECT().
						WriteMessage(repository.Message{
							Time:    timeAsk.Unix(),
							Message: "Тикер GAZP не найден",
						}).
						Return(nil)
					return m
				}(),
				timer: func() marketdata.Timer {
					m := mock_api.NewMockTimer(gomock.NewController(t))
					m.EXPECT().Now().Return(timeAsk)
					return m
				}(),
			},
			tiker:       gazp.Tiker,
			expectedErr: marketdata.ErrInstrumentNotFound,
		},
		{
			name: "err on send tiker info message",
			fields: appFields{
				repo: func() marketdata.MarketRepo {
					m := mock_api.NewMockMarketRepo(gomock.NewController(t))
					m.EXPECT().
						ReadInstrument(repository.Instrument{
							Tiker:        gazp.Tiker,
							ClassCode:    sql.NullString{Valid: true, String: ""},
							Exchange:     sql.NullString{Valid: true, String: ""},
							RealExchange: sql.NullString{Valid: true, String: ""},
						}).
						Return(repository.Instrument{}, sql.ErrNoRows)
					m.EXPECT().
						WriteMessage(repository.Message{
							Time:    timeAsk.Unix(),
							Message: "Тикер GAZP не найден",
						}).
						Return(errSome)
					return m
				}(),
				timer: func() marketdata.Timer {
					m := mock_api.NewMockTimer(gomock.NewController(t))
					m.EXPECT().Now().Return(timeAsk)
					return m
				}(),
			},
			tiker:       gazp.Tiker,
			expectedErr: marketdata.ErrInstrumentNotFound,
		},
		{
			name: "tiker get err on tiker info",
			fields: appFields{
				repo: func() marketdata.MarketRepo {
					m := mock_api.NewMockMarketRepo(gomock.NewController(t))
					m.EXPECT().
						ReadInstrument(repository.Instrument{
							Tiker:        gazp.Tiker,
							ClassCode:    sql.NullString{Valid: true, String: ""},
							Exchange:     sql.NullString{Valid: true, String: ""},
							RealExchange: sql.NullString{Valid: true, String: ""},
						}).
						Return(repository.Instrument{}, errSome)
					return m
				}(),
				timer: func() marketdata.Timer {
					m := mock_api.NewMockTimer(gomock.NewController(t))
					return m
				}(),
			},
			tiker:       gazp.Tiker,
			expectedErr: errSome,
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			app := marketdata.NewApp(test.fields.tAPI, test.fields.repo, test.fields.timer)
			err := app.SendTikerInfo(test.tiker)
			if test.expectedErr == nil {
				require.Nil(t, err)
			} else {
				require.ErrorIs(t, err, test.expectedErr)
			}
		})
	}
}

func TestSetBell(t *testing.T) {
	t.Parallel()

	type args struct {
		instrument *marketdata.Instrument
		target     float64
	}

	type TestSetBell struct {
		name        string
		fields      appFields
		args        args
		expectedErr error
	}

	sber := instrumentSBER()
	sberInactive := sber
	sberInactive.Active = false
	pricesSber := instrumentPrices()
	prices := []*investapi.LastPrice{
		{
			Figi:  sber.Figi,
			Price: quotationFromAPI(100, 0),
			Time:  timestamppb.Now(),
		},
	}
	messageTime := time.Now()

	tests := []TestSetBell{
		{
			name: "valid bell",
			fields: appFields{
				repo: func() marketdata.MarketRepo {
					m := mock_api.NewMockMarketRepo(gomock.NewController(t))
					m.EXPECT().ReadInstrument(repository.Instrument{
						Tiker:        sber.Tiker,
						ClassCode:    sql.NullString{Valid: true, String: ""},
						Exchange:     sql.NullString{Valid: true, String: ""},
						RealExchange: sql.NullString{Valid: true, String: ""},
					}).Return(sber, nil)
					m.EXPECT().WriteBell(repository.Bell{
						Figi:      sber.Figi,
						Target:    100.23,
						FromBelow: true,
					}).Return(nil)
					m.EXPECT().
						WriteMessage(repository.Message{
							Time:    messageTime.Unix(),
							Message: "Для SBER установлена цель 100.23",
						}).
						Return(nil)
					return m
				}(),
				tAPI: func() marketdata.TinkoffAPI {
					m := mock_api.NewMockTinkoffAPI(gomock.NewController(t))
					m.EXPECT().GetLastPrices([]string{sber.Figi}).Return(prices, nil)
					return m
				}(),
				timer: func() marketdata.Timer {
					m := mock_api.NewMockTimer(gomock.NewController(t))
					m.EXPECT().Now().Return(messageTime)
					return m
				}(),
			},
			args: args{
				instrument: &marketdata.Instrument{
					Tiker: sber.Tiker,
				},
				target: 100.23,
			},
			expectedErr: nil,
		},
		{
			name: "valid bell, inactive instrument",
			fields: appFields{
				repo: func() marketdata.MarketRepo {
					m := mock_api.NewMockMarketRepo(gomock.NewController(t))
					m.EXPECT().ReadInstrument(repository.Instrument{
						Tiker:        sber.Tiker,
						ClassCode:    sql.NullString{Valid: true, String: ""},
						Exchange:     sql.NullString{Valid: true, String: ""},
						RealExchange: sql.NullString{Valid: true, String: ""},
					}).Return(sberInactive, nil)
					m.EXPECT().
						WriteInstrument(sber).
						Return(nil)
					m.EXPECT().WriteCandle(repository.Candle{
						Figi:   sber.Figi,
						Time:   pricesSber[0].Time.Seconds,
						Open:   float64(pricesSber[0].Open.Units),
						Close:  float64(pricesSber[0].Close.Units),
						Low:    float64(pricesSber[0].Low.Units),
						High:   float64(pricesSber[0].High.Units),
						Volume: pricesSber[0].Volume,
					}).Return(nil)
					m.EXPECT().WriteCandle(repository.Candle{
						Figi:   sber.Figi,
						Time:   pricesSber[1].Time.Seconds,
						Open:   float64(pricesSber[1].Open.Units),
						Close:  float64(pricesSber[1].Close.Units),
						Low:    float64(pricesSber[1].Low.Units),
						High:   float64(pricesSber[1].High.Units),
						Volume: pricesSber[1].Volume,
					}).Return(nil)
					m.EXPECT().WriteBell(repository.Bell{
						Figi:      sber.Figi,
						Target:    100.23,
						FromBelow: true,
					}).Return(nil)
					m.EXPECT().
						WriteMessage(repository.Message{
							Time:    messageTime.Unix(),
							Message: "Для SBER установлена цель 100.23",
						}).
						Return(nil)
					return m
				}(),
				tAPI: func() marketdata.TinkoffAPI {
					m := mock_api.NewMockTinkoffAPI(gomock.NewController(t))
					m.EXPECT().
						GetCandles(sber.Figi,
							gomock.Any(),
							gomock.Any(),
							investapi.CandleInterval_CANDLE_INTERVAL_DAY).
						Return(pricesSber, nil)
					m.EXPECT().GetLastPrices([]string{sber.Figi}).Return(prices, nil)
					return m
				}(),
				timer: func() marketdata.Timer {
					m := mock_api.NewMockTimer(gomock.NewController(t))
					m.EXPECT().Now().Return(messageTime).Times(2)
					return m
				}(),
			},
			args: args{
				instrument: &marketdata.Instrument{
					Tiker: sber.Tiker,
				},
				target: 100.23,
			},
			expectedErr: nil,
		},
		{
			name: "err on active bell instrument, inactive instrument",
			fields: appFields{
				repo: func() marketdata.MarketRepo {
					m := mock_api.NewMockMarketRepo(gomock.NewController(t))
					m.EXPECT().ReadInstrument(repository.Instrument{
						Tiker:        sber.Tiker,
						ClassCode:    sql.NullString{Valid: true, String: ""},
						Exchange:     sql.NullString{Valid: true, String: ""},
						RealExchange: sql.NullString{Valid: true, String: ""},
					}).Return(sberInactive, nil)
					m.EXPECT().
						WriteInstrument(sber).
						Return(errSome)
					return m
				}(),
				tAPI: func() marketdata.TinkoffAPI {
					m := mock_api.NewMockTinkoffAPI(gomock.NewController(t))
					return m
				}(),
				timer: func() marketdata.Timer {
					m := mock_api.NewMockTimer(gomock.NewController(t))
					return m
				}(),
			},
			args: args{
				instrument: &marketdata.Instrument{
					Tiker: sber.Tiker,
				},
				target: 100.23,
			},
			expectedErr: errSome,
		},
		{
			name: "on send message bell error",
			fields: appFields{
				repo: func() marketdata.MarketRepo {
					m := mock_api.NewMockMarketRepo(gomock.NewController(t))
					m.EXPECT().ReadInstrument(repository.Instrument{
						Tiker:        sber.Tiker,
						ClassCode:    sql.NullString{Valid: true, String: ""},
						Exchange:     sql.NullString{Valid: true, String: ""},
						RealExchange: sql.NullString{Valid: true, String: ""},
					}).Return(sber, nil)
					m.EXPECT().WriteBell(repository.Bell{
						Figi:      sber.Figi,
						Target:    100.23,
						FromBelow: true,
					}).Return(nil)
					m.EXPECT().
						WriteMessage(repository.Message{
							Time:    messageTime.Unix(),
							Message: "Для SBER установлена цель 100.23",
						}).
						Return(errSome)
					return m
				}(),
				tAPI: func() marketdata.TinkoffAPI {
					m := mock_api.NewMockTinkoffAPI(gomock.NewController(t))
					m.EXPECT().GetLastPrices([]string{sber.Figi}).Return(prices, nil)
					return m
				}(),
				timer: func() marketdata.Timer {
					m := mock_api.NewMockTimer(gomock.NewController(t))
					m.EXPECT().Now().Return(messageTime)
					return m
				}(),
			},
			args: args{
				instrument: &marketdata.Instrument{
					Tiker: sber.Tiker,
				},
				target: 100.23,
			},
			expectedErr: errSome,
		},
		{
			name: "no last price for bell",
			fields: appFields{
				repo: func() marketdata.MarketRepo {
					m := mock_api.NewMockMarketRepo(gomock.NewController(t))
					m.EXPECT().ReadInstrument(repository.Instrument{
						Tiker:        sber.Tiker,
						ClassCode:    sql.NullString{Valid: true, String: ""},
						Exchange:     sql.NullString{Valid: true, String: ""},
						RealExchange: sql.NullString{Valid: true, String: ""},
					}).Return(sber, nil)
					return m
				}(),
				tAPI: func() marketdata.TinkoffAPI {
					m := mock_api.NewMockTinkoffAPI(gomock.NewController(t))
					m.EXPECT().GetLastPrices([]string{sber.Figi}).Return(nil, nil)
					return m
				}(),
				timer: func() marketdata.Timer {
					m := mock_api.NewMockTimer(gomock.NewController(t))
					return m
				}(),
			},
			args: args{
				instrument: &marketdata.Instrument{
					Tiker: sber.Tiker,
				},
				target: 100.23,
			},
			expectedErr: marketdata.ErrNoLastPrice,
		},
		{
			name: "err on get last price",
			fields: appFields{
				repo: func() marketdata.MarketRepo {
					m := mock_api.NewMockMarketRepo(gomock.NewController(t))
					m.EXPECT().ReadInstrument(repository.Instrument{
						Tiker:        sber.Tiker,
						ClassCode:    sql.NullString{Valid: true, String: ""},
						Exchange:     sql.NullString{Valid: true, String: ""},
						RealExchange: sql.NullString{Valid: true, String: ""},
					}).Return(sber, nil)
					return m
				}(),
				tAPI: func() marketdata.TinkoffAPI {
					m := mock_api.NewMockTinkoffAPI(gomock.NewController(t))
					m.EXPECT().GetLastPrices([]string{sber.Figi}).Return(nil, errSome)
					return m
				}(),
				timer: func() marketdata.Timer {
					m := mock_api.NewMockTimer(gomock.NewController(t))
					return m
				}(),
			},
			args: args{
				instrument: &marketdata.Instrument{
					Tiker: sber.Tiker,
				},
				target: 100.23,
			},
			expectedErr: errSome,
		},
		{
			name: "err on get bell instrument",
			fields: appFields{
				repo: func() marketdata.MarketRepo {
					m := mock_api.NewMockMarketRepo(gomock.NewController(t))
					m.EXPECT().
						ReadInstrument(repository.Instrument{
							ClassCode:    sql.NullString{Valid: true, String: ""},
							Exchange:     sql.NullString{Valid: true, String: ""},
							RealExchange: sql.NullString{Valid: true, String: ""},
						}).
						Return(repository.Instrument{}, errSome)
					return m
				}(),
				tAPI: func() marketdata.TinkoffAPI {
					m := mock_api.NewMockTinkoffAPI(gomock.NewController(t))
					return m
				}(),
				timer: func() marketdata.Timer {
					m := mock_api.NewMockTimer(gomock.NewController(t))
					return m
				}(),
			},
			args: args{
				instrument: &marketdata.Instrument{},
				target:     100.23,
			},
			expectedErr: errSome,
		},
		{
			name: "no bell instrument",
			fields: appFields{
				repo: func() marketdata.MarketRepo {
					m := mock_api.NewMockMarketRepo(gomock.NewController(t))
					m.EXPECT().
						ReadInstrument(repository.Instrument{
							ClassCode:    sql.NullString{Valid: true, String: ""},
							Exchange:     sql.NullString{Valid: true, String: ""},
							RealExchange: sql.NullString{Valid: true, String: ""},
						}).
						Return(repository.Instrument{}, sql.ErrNoRows)
					return m
				}(),
				tAPI: func() marketdata.TinkoffAPI {
					m := mock_api.NewMockTinkoffAPI(gomock.NewController(t))
					return m
				}(),
				timer: func() marketdata.Timer {
					m := mock_api.NewMockTimer(gomock.NewController(t))
					return m
				}(),
			},
			args: args{
				instrument: &marketdata.Instrument{},
				target:     100.23,
			},
			expectedErr: marketdata.ErrInstrumentNotFound,
		},
		{
			name: "on read bell instrument error",
			fields: appFields{
				repo: func() marketdata.MarketRepo {
					m := mock_api.NewMockMarketRepo(gomock.NewController(t))
					m.EXPECT().
						ReadInstrument(repository.Instrument{
							Tiker:        sber.Tiker,
							ClassCode:    sql.NullString{Valid: true, String: ""},
							Exchange:     sql.NullString{Valid: true, String: ""},
							RealExchange: sql.NullString{Valid: true, String: ""},
						}).
						Return(repository.Instrument{}, errSome)
					return m
				}(),
				tAPI: func() marketdata.TinkoffAPI {
					m := mock_api.NewMockTinkoffAPI(gomock.NewController(t))
					return m
				}(),
				timer: func() marketdata.Timer {
					m := mock_api.NewMockTimer(gomock.NewController(t))
					return m
				}(),
			},
			args: args{
				instrument: &marketdata.Instrument{
					Tiker: sber.Tiker,
				},
				target: 100.23,
			},
			expectedErr: errSome,
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			app := marketdata.NewApp(test.fields.tAPI, test.fields.repo, test.fields.timer)
			err := app.SetBell(test.args.instrument, test.args.target)
			if test.expectedErr == nil {
				require.Nil(t, err)
			} else {
				require.ErrorIs(t, err, test.expectedErr)
			}
		})
	}
}

func TestTikerExist(t *testing.T) {
	t.Parallel()

	type TestExist struct {
		name           string
		fields         appFields
		tiker          string
		expectedResult bool
	}

	sber := instrumentSBER()

	tests := []TestExist{
		{
			name:  "tiker exist",
			tiker: sber.Tiker,
			fields: appFields{
				repo: func() marketdata.MarketRepo {
					m := mock_api.NewMockMarketRepo(gomock.NewController(t))
					m.EXPECT().
						ReadInstrument(repository.Instrument{
							Tiker:        sber.Tiker,
							ClassCode:    sql.NullString{Valid: true, String: ""},
							Exchange:     sql.NullString{Valid: true, String: ""},
							RealExchange: sql.NullString{Valid: true, String: ""},
						}).
						Return(sber, nil)
					return m
				}(),
			},
			expectedResult: true,
		},
		{
			name:  "tiker doesn't exist",
			tiker: "SBER1",
			fields: appFields{
				repo: func() marketdata.MarketRepo {
					m := mock_api.NewMockMarketRepo(gomock.NewController(t))
					m.EXPECT().
						ReadInstrument(repository.Instrument{
							Tiker:        "SBER1",
							ClassCode:    sql.NullString{Valid: true, String: ""},
							Exchange:     sql.NullString{Valid: true, String: ""},
							RealExchange: sql.NullString{Valid: true, String: ""},
						}).
						Return(repository.Instrument{}, sql.ErrNoRows)
					return m
				}(),
			},
			expectedResult: false,
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			app := marketdata.NewApp(test.fields.tAPI, test.fields.repo, test.fields.timer)
			exist := app.TikerExist(test.tiker)
			require.Equal(t, exist, test.expectedResult)
		})
	}
}

func TestSetActiveByTiker(t *testing.T) {
	t.Parallel()

	type args struct {
		tiker  string
		active bool
	}

	type testSetActive struct {
		name        string
		fields      appFields
		args        args
		expectedErr error
	}

	sber := instrumentSBER()
	messageTime := time.Now()
	pricesSber := instrumentPrices()

	tests := []testSetActive{
		{
			name: "valit tiker active - true",
			fields: appFields{
				repo: func() marketdata.MarketRepo {
					m := mock_api.NewMockMarketRepo(gomock.NewController(t))
					m.EXPECT().
						ReadInstrument(repository.Instrument{
							Tiker:        sber.Tiker,
							ClassCode:    sql.NullString{Valid: true, String: ""},
							Exchange:     sql.NullString{Valid: true, String: ""},
							RealExchange: sql.NullString{Valid: true, String: ""},
						}).
						Return(sber, nil)
					m.EXPECT().WriteInstrument(sber).Return(nil)
					m.EXPECT().WriteCandle(repository.Candle{
						Figi:   sber.Figi,
						Time:   pricesSber[0].Time.Seconds,
						Open:   float64(pricesSber[0].Open.Units),
						Close:  float64(pricesSber[0].Close.Units),
						Low:    float64(pricesSber[0].Low.Units),
						High:   float64(pricesSber[0].High.Units),
						Volume: pricesSber[0].Volume,
					}).Return(nil)
					m.EXPECT().WriteCandle(repository.Candle{
						Figi:   sber.Figi,
						Time:   pricesSber[1].Time.Seconds,
						Open:   float64(pricesSber[1].Open.Units),
						Close:  float64(pricesSber[1].Close.Units),
						Low:    float64(pricesSber[1].Low.Units),
						High:   float64(pricesSber[1].High.Units),
						Volume: pricesSber[1].Volume,
					}).Return(nil)
					return m
				}(),
				tAPI: func() marketdata.TinkoffAPI {
					m := mock_api.NewMockTinkoffAPI(gomock.NewController(t))
					m.EXPECT().
						GetCandles(sber.Figi,
							gomock.Any(),
							gomock.Any(),
							investapi.CandleInterval_CANDLE_INTERVAL_DAY).
						Return(pricesSber, nil)
					return m
				}(),
				timer: func() marketdata.Timer {
					m := mock_api.NewMockTimer(gomock.NewController(t))
					m.EXPECT().Now().Return(messageTime)
					return m
				}(),
			},
			args: args{
				tiker:  sber.Tiker,
				active: true,
			},
			expectedErr: nil,
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			app := marketdata.NewApp(test.fields.tAPI, test.fields.repo, test.fields.timer)
			err := app.SetActiveByTiker(test.args.tiker, test.args.active)
			if test.expectedErr == nil {
				require.Nil(t, err)
			} else {
				require.ErrorIs(t, err, test.expectedErr)
			}
		})
	}
}

func TestSetLevelByTiker(t *testing.T) {
	t.Parallel()

	type args struct {
		tiker string
		level int8
	}

	type testSetLevel struct {
		name        string
		fields      appFields
		args        args
		expectedErr error
	}

	sber := instrumentSBER()
	sberLevel11 := sber
	sberLevel11.TriggerLevel = 11

	tests := []testSetLevel{
		{
			name: "valit tiker level - 11",
			fields: appFields{
				repo: func() marketdata.MarketRepo {
					m := mock_api.NewMockMarketRepo(gomock.NewController(t))
					m.EXPECT().
						ReadInstrument(repository.Instrument{
							Tiker:        sber.Tiker,
							ClassCode:    sql.NullString{Valid: true, String: ""},
							Exchange:     sql.NullString{Valid: true, String: ""},
							RealExchange: sql.NullString{Valid: true, String: ""},
						}).
						Return(sber, nil)
					m.EXPECT().WriteInstrument(sberLevel11).Return(nil)
					return m
				}(),
			},
			args: args{
				tiker: sber.Tiker,
				level: 11,
			},
			expectedErr: nil,
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			app := marketdata.NewApp(test.fields.tAPI, test.fields.repo, test.fields.timer)
			err := app.SetLevelByTiker(test.args.tiker, test.args.level)
			if test.expectedErr == nil {
				require.Nil(t, err)
			} else {
				require.ErrorIs(t, err, test.expectedErr)
			}
		})
	}
}

func TestSendMessage(t *testing.T) {
	type testMessage struct {
		name    string
		fields  appFields
		message string
		hasErr  bool
	}

	messageTime := time.Now()

	tests := []testMessage{
		{
			name: "valid send message",
			fields: appFields{
				repo: func() marketdata.MarketRepo {
					m := mock_api.NewMockMarketRepo(gomock.NewController(t))
					m.EXPECT().
						WriteMessage(repository.Message{
							Message: "la-la-lorem",
							Time:    messageTime.Unix(),
						}).
						Return(nil)
					return m
				}(),
				timer: func() marketdata.Timer {
					m := mock_api.NewMockTimer(gomock.NewController(t))
					m.EXPECT().Now().Return(messageTime)
					return m
				}(),
			},
			message: "la-la-lorem",
			hasErr:  false,
		},
		{
			name: "error on send message",
			fields: appFields{
				repo: func() marketdata.MarketRepo {
					m := mock_api.NewMockMarketRepo(gomock.NewController(t))
					m.EXPECT().
						WriteMessage(repository.Message{
							Message: "la-la-lorem",
							Time:    messageTime.Unix(),
						}).
						Return(errSome)
					return m
				}(),
				timer: func() marketdata.Timer {
					m := mock_api.NewMockTimer(gomock.NewController(t))
					m.EXPECT().Now().Return(messageTime)
					return m
				}(),
			},
			message: "la-la-lorem",
			hasErr:  true,
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			app := marketdata.NewApp(test.fields.tAPI, test.fields.repo, test.fields.timer)
			err := app.SendMessage(test.message)
			require.Equal(t, err != nil, test.hasErr)
		})
	}
}

func instrumentSBER() repository.Instrument {
	return shareToInstrument(shareSBER())
}

func instrumentGAZP() repository.Instrument {
	return shareToInstrument(shareGAZP())
}

func shareToInstrument(share *investapi.Share) repository.Instrument {
	return repository.Instrument{
		Figi:         share.Figi,
		Name:         share.Name,
		Tiker:        share.Ticker,
		Currency:     share.Currency,
		MinPrice:     share.MinPriceIncrement.Nano,
		Lot:          share.Lot,
		ClassCode:    sql.NullString{Valid: true, String: share.ClassCode},
		Exchange:     sql.NullString{Valid: true, String: share.Exchange},
		RealExchange: sql.NullString{Valid: true, String: share.RealExchange.String()},
		TriggerLevel: 7,
		Active:       true,
	}
}

func shareSBER() *investapi.Share {
	return &investapi.Share{
		Figi:      "BBG004730N88",
		Ticker:    "SBER",
		ClassCode: "TQBR",
		Isin:      "RU0009029540",
		Lot:       10,
		Currency:  "rub",
		Klong: &investapi.Quotation{
			Units: 2,
			Nano:  0,
		},
		Kshort: &investapi.Quotation{
			Units: 2,
			Nano:  0,
		},
		Dlong: &investapi.Quotation{
			Units: 0,
			Nano:  360000000,
		},
		Dshort: &investapi.Quotation{
			Units: 0,
			Nano:  440000000,
		},
		DlongMin: &investapi.Quotation{
			Units: 0,
			Nano:  200000000,
		},
		DshortMin: &investapi.Quotation{
			Units: 0,
			Nano:  200000000,
		},
		ShortEnabledFlag:  true,
		Name:              "Сбербанк",
		Exchange:          "MOEX_EVENING_WEEKEND",
		IpoDate:           timeFromStringAPI("1998-12-30T00:00:00Z"),
		IssueSize:         3673512900,
		CountryOfRisk:     "RU",
		CountryOfRiskName: "Российская Федерация",
		Sector:            "energy",
		IssueSizePlan:     3673512900,
		Nominal: &investapi.MoneyValue{
			Currency: "rub",
			Units:    5,
			Nano:     0,
		},
		TradingStatus:     investapi.SecurityTradingStatus_SECURITY_TRADING_STATUS_SESSION_OPEN,
		OtcFlag:           false,
		BuyAvailableFlag:  true,
		SellAvailableFlag: true,
		DivYieldFlag:      true,
		ShareType:         investapi.ShareType_SHARE_TYPE_COMMON,
		MinPriceIncrement: &investapi.Quotation{
			Units: 0,
			Nano:  10000000,
		},
		ApiTradeAvailableFlag: true,
		Uid:                   "962e2a95-02a9-4171-abd7-aa198dbe643b",
		RealExchange:          investapi.RealExchange_REAL_EXCHANGE_MOEX,
	}
}

func shareGAZP() *investapi.Share {
	return &investapi.Share{
		Figi:      "BBG004730RP0",
		Ticker:    "GAZP",
		ClassCode: "TQBR",
		Isin:      "RU0007661625",
		Lot:       10,
		Currency:  "rub",
		Klong: &investapi.Quotation{
			Units: 2,
			Nano:  0,
		},
		Kshort: &investapi.Quotation{
			Units: 2,
			Nano:  0,
		},
		Dlong: &investapi.Quotation{
			Units: 0,
			Nano:  360000000,
		},
		Dshort: &investapi.Quotation{
			Units: 0,
			Nano:  440000000,
		},
		DlongMin: &investapi.Quotation{
			Units: 0,
			Nano:  200000000,
		},
		DshortMin: &investapi.Quotation{
			Units: 0,
			Nano:  200000000,
		},
		ShortEnabledFlag:  true,
		Name:              "Газпром",
		Exchange:          "MOEX_EVENING_WEEKEND",
		IpoDate:           timeFromStringAPI("1998-12-30T00:00:00Z"),
		IssueSize:         23673512900,
		CountryOfRisk:     "RU",
		CountryOfRiskName: "Российская Федерация",
		Sector:            "energy",
		IssueSizePlan:     23673512900,
		Nominal: &investapi.MoneyValue{
			Currency: "rub",
			Units:    5,
			Nano:     0,
		},
		TradingStatus:     investapi.SecurityTradingStatus_SECURITY_TRADING_STATUS_SESSION_OPEN,
		OtcFlag:           false,
		BuyAvailableFlag:  true,
		SellAvailableFlag: true,
		DivYieldFlag:      true,
		ShareType:         investapi.ShareType_SHARE_TYPE_COMMON,
		MinPriceIncrement: &investapi.Quotation{
			Units: 0,
			Nano:  10000000,
		},
		ApiTradeAvailableFlag: true,
		Uid:                   "962e2a95-02a9-4171-abd7-aa198dbe643a",
		RealExchange:          investapi.RealExchange_REAL_EXCHANGE_MOEX,
	}
}

func instrumentPrices() []*investapi.HistoricCandle {
	now := time.Now()
	var day int64 = 86400
	return []*investapi.HistoricCandle{
		{
			Open:       quotationFromAPI(100+rand.Int63n(100), 0),
			Close:      quotationFromAPI(100+rand.Int63n(100), 0),
			Low:        quotationFromAPI(rand.Int63n(100), 0),
			High:       quotationFromAPI(200+rand.Int63n(100), 0),
			Volume:     rand.Int63n(1000000),
			IsComplete: true,
			Time: &timestamppb.Timestamp{
				Seconds: now.Unix() - day,
			},
		},
		{
			Open:       quotationFromAPI(100+rand.Int63n(100), 0),
			Close:      quotationFromAPI(100+rand.Int63n(100), 0),
			Low:        quotationFromAPI(rand.Int63n(100), 0),
			High:       quotationFromAPI(200+rand.Int63n(100), 0),
			Volume:     rand.Int63n(1000000),
			IsComplete: false,
			Time: &timestamppb.Timestamp{
				Seconds: now.Unix(),
			},
		},
	}
}

func quotationFromAPI(units int64, nano int32) *investapi.Quotation {
	return &investapi.Quotation{
		Units: units,
		Nano:  nano,
	}
}

func timeFromStringAPI(source string) *timestamppb.Timestamp {
	output, err := time.Parse(time.RFC3339, "1998-12-30T00:00:00Z")
	if err != nil {
		log.Panicln(err)
	}

	return &timestamppb.Timestamp{
		Seconds: output.Unix(),
	}
}

func TestDefTimerCheck(t *testing.T) {
	t.Parallel()

	timer := marketdata.DefaultTimer()
	now := timer.Now().Unix()
	realNow := timer.Now().Unix()
	require.LessOrEqual(t, math.Abs(float64(now-realNow)), 1.0)
}
