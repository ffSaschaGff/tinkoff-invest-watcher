package marketdata

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"math"
	"strconv"
	"sync"
	"time"

	"github.com/ssummers02/invest-api-go-sdk/pkg/investapi"
	"gitlab.com/ffSaschaGff/tinkoff-invest-watcher/internal/optional"
	"gitlab.com/ffSaschaGff/tinkoff-invest-watcher/internal/repository"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// magic const 24h.
const hoursInDay = 24

// how many days need to load on init.
const loadDaysAgo = 100

type marketApp struct {
	tinkoffAPI TinkoffAPI
	repo       MarketRepo
	timer      Timer
}

var (
	ErrInstrumentNotFound = errors.New("instrument doesn't exsist")
	ErrNoLastPrice        = errors.New("can't get last price")
	eventsTypeMapBlock    = map[int][]int{
		0: {1, 2},
		1: {2},
		3: {4, 5},
		4: {5},
	}
)

// MarketApp - this service.
type MarketApp interface {
	// InitMarketData - init shares.
	InitMarketData() error
	LoadPrices()
	ClearGarbage()
	UpdateAggregates()
	GetNewEvents() ([]string, error)
	SendTikerInfo(tiker string) error
	SetBell(instrument *Instrument, target float64) error
	TikerExist(tiker string) bool
	SetActiveByTiker(tiker string, active bool) error
	SetLevelByTiker(tiker string, level int8) error
	SendMessage(message string) error
	ForceUpdate() error
}

// TinkoffAPI - tinkoff api endpoint.
type TinkoffAPI interface {
	Shares(status investapi.InstrumentStatus) ([]*investapi.Share, error)
	GetLastPrices(figi []string) ([]*investapi.LastPrice, error)
	GetCandles(figi string,
		from *timestamppb.Timestamp,
		to *timestamppb.Timestamp,
		interval investapi.CandleInterval) ([]*investapi.HistoricCandle, error)
}

// MarketRepo - abstract data provider.
type MarketRepo interface {
	ReadInstrument(instrument repository.Instrument) (repository.Instrument, error)
	WriteInstrument(instrument repository.Instrument) error
	GetActiveInstruments() ([]repository.Instrument, error)
	WriteCandle(candle repository.Candle) error
	WriteBell(bell repository.Bell) error
	WriteMessage(message repository.Message) error
	// GetBells - get bells by instrument id.
	GetBells(figi string) ([]float64, error)
	// GetMessages - get all new messages.
	GetMessages() ([]string, error)
	// ClearGarbage - delete all old data.
	ClearGarbage()
	// UpdateAggregates - update instruments aggregates.
	UpdateAggregates()
	// Events.
	NewEventsBigChange() []repository.EventBigChange
	NewEventsBigVolume() []repository.EventBigVolume
	NewEventsBell() []repository.EventBell
	ListCandles(filter repository.ListCandlesFilter) (map[string][]repository.Candle, error)
	ListEvents(filter repository.ListEventsFilter) (map[string][]repository.Message, error)
}

type Timer interface {
	Now() time.Time
}

// Instrument - instrument (share, bond).
type Instrument struct {
	Figi         string
	Name         string
	Tiker        string
	Currency     string
	ClassCode    string
	Exchange     string
	RealExchange string
	MinPrice     int32
	Lot          int32
	TriggerLevel int8
	Active       bool
}

// Candle - type for day candle.
type Candle struct {
	Figi   string
	Tiker  string
	Time   int64
	Open   float64
	Close  float64
	Low    float64
	High   float64
	Volume int64
}

// Bell - type for bell.
type Bell struct {
	Figi      string
	Tiker     string
	Target    float64
	FromBelow bool
}

// Message - message to send out.
type Message struct {
	Figi    string
	Message string
	Time    int64
	MsgType MessageTypes
	Sended  bool
}

func NewApp(
	tinkoffAPI TinkoffAPI,
	repo MarketRepo,
	timer Timer,
) MarketApp {
	return marketApp{
		tinkoffAPI: tinkoffAPI,
		repo:       repo,
		timer:      timer,
	}
}

type defTimer struct{}

func (timer defTimer) Now() time.Time {
	return time.Now()
}

func DefaultTimer() Timer {
	return defTimer{}
}

// ForceUpdate - force updale like it all tikets was added right now.
func (app marketApp) ForceUpdate() error {
	app.loadPrices(60)
	app.UpdateAggregates()

	return nil
}

// InitMarketData - load all inftruments fron API.
func (app marketApp) InitMarketData() error {
	// load all shares
	figi, err := app.tinkoffAPI.Shares(investapi.InstrumentStatus_INSTRUMENT_STATUS_ALL)
	if err != nil {
		return err
	}

	return app.writeFigisToDB(figi)
}

// LoadPrices - load prices of active instruments.
func (app marketApp) LoadPrices() {
	app.loadPrices(1)
}

func (app marketApp) loadPrices(days int) {
	activeFigi, err := app.getActiveInstruments()
	if err != nil {
		log.Println(err.Error())

		return
	}

	cooldown := 60
	for len(activeFigi) > 0 {
		packSize := 50
		if len(activeFigi) < packSize {
			packSize = len(activeFigi)
		}
		now := app.timer.Now()
		readyToLoad := activeFigi[:packSize]
		activeFigi = activeFigi[packSize:]
		rowStart := now.Unix()
		var waitPrices sync.WaitGroup
		waitPrices.Add(len(readyToLoad))
		for _, instrument := range readyToLoad {
			go app.loadPriceParallel(instrument, days, &waitPrices)
		}
		waitPrices.Wait()

		secodDelta := rowStart + int64(cooldown) - now.Unix()
		if secodDelta > 0 && len(activeFigi) > 0 {
			time.Sleep(time.Duration(secodDelta) * time.Second)
		}
	}

	app.writeEvents()
}

// ClearGarbage - delete all old data.
func (app marketApp) ClearGarbage() {
	app.repo.ClearGarbage()
}

// SendMessage - send message.
func (app marketApp) SendMessage(message string) error {
	dbMessage := repository.Message{
		Message: message,
		Time:    app.timer.Now().Unix(),
	}
	err := app.repo.WriteMessage(dbMessage)

	return err
}

// SetBell - set bell for instrument. It get current price and set bell.
func (app marketApp) SetBell(instrument *Instrument, target float64) error {
	dbInstrument := instrument.getDBObject()
	dbInstrument, err := app.repo.ReadInstrument(dbInstrument)
	if errors.Is(err, sql.ErrNoRows) {
		return ErrInstrumentNotFound
	} else if err != nil {
		return err
	}

	instrument.readDBObject(dbInstrument)

	if !instrument.Active {
		err = app.setActive(*instrument, true)
		if err != nil {
			return err
		}
	}

	prices, err := app.tinkoffAPI.GetLastPrices([]string{instrument.Figi})
	if err != nil {
		return err
	}

	lastPrice := 0.0
	for _, price := range prices {
		if price.Figi == instrument.Figi {
			lastPrice = getQuotationValue(price.Price)

			break
		}
	}

	if lastPrice == 0.0 {
		return ErrNoLastPrice
	}

	bell := Bell{}
	bell.Figi = instrument.Figi
	bell.Target = target
	bell.FromBelow = lastPrice < target

	err = app.repo.WriteBell(bell.getDBObject())
	if err == nil {
		message := fmt.Sprintf("Для %s установлена цель %s", instrument.Tiker, formatDouble(bell.Target))
		err := app.SendMessage(message)
		if err != nil {
			return err
		}
	}

	return err
}

func (app marketApp) SendTikerInfo(tiker string) error {
	instrument := Instrument{}
	instrument.Tiker = tiker
	dbInstrument := instrument.getDBObject()
	dbInstrument, err := app.repo.ReadInstrument(dbInstrument)
	if errors.Is(err, sql.ErrNoRows) {
		errMsg := fmt.Sprintf("Тикер %s не найден", tiker)
		err := app.SendMessage(errMsg)
		if err != nil {
			log.Println(err)
		}

		return fmt.Errorf("%w: %s", ErrInstrumentNotFound, errMsg)
	} else if err != nil {
		return err
	}

	instrument.readDBObject(dbInstrument)

	active := "не отслеживается"
	if instrument.Active {
		active = "отслеживается"
	}
	message := fmt.Sprintf("%s: %s %s\n", instrument.Tiker, instrument.Name, active)
	bells, err := app.repo.GetBells(instrument.Figi)
	if err != nil {
		return err
	}
	if len(bells) == 0 {
		message += "Колокольчиков нет"
	} else {
		message += "Колокольчики:"
		for _, bell := range bells {
			formatedBell := formatDouble(bell)
			message += "\n" + formatedBell
		}
	}
	err = app.SendMessage(message)

	return err
}

// UpdateAggregates - update aggregates of figi i.e. avg volume.
func (app marketApp) UpdateAggregates() {
	app.repo.UpdateAggregates()
}

func (app marketApp) SetActiveByTiker(tiker string, active bool) error {
	instrument := Instrument{
		Tiker: tiker,
	}

	dbInstrument := instrument.getDBObject()
	dbInstrument, err := app.repo.ReadInstrument(dbInstrument)
	if errors.Is(err, sql.ErrNoRows) {
		return ErrInstrumentNotFound
	} else if err != nil {
		return err
	}

	instrument.readDBObject(dbInstrument)

	return app.setActive(instrument, active)
}

func (app marketApp) SetLevelByTiker(tiker string, level int8) error {
	instrument := Instrument{
		Tiker: tiker,
	}

	dbInstrument := instrument.getDBObject()
	dbInstrument, err := app.repo.ReadInstrument(dbInstrument)
	if errors.Is(err, sql.ErrNoRows) {
		return ErrInstrumentNotFound
	} else if err != nil {
		return err
	}

	instrument.readDBObject(dbInstrument)
	instrument.TriggerLevel = level

	return app.repo.WriteInstrument(instrument.getDBObject())
}

func (app marketApp) TikerExist(tiker string) bool {
	instrument := Instrument{
		Tiker: tiker,
	}
	dbInstrument := instrument.getDBObject()
	_, err := app.repo.ReadInstrument(dbInstrument)

	return err == nil
}

func (app marketApp) writeFigisToDB(figi []*investapi.Share) error {
	for _, oneFigi := range figi {
		// convert every *investapi.Share to Instrument
		minPrice := minPrice(oneFigi)

		instrument := Instrument{}
		instrument.Figi = oneFigi.Figi
		dbInstrument := instrument.getDBObject()
		dbInstrument, err := app.repo.ReadInstrument(dbInstrument)
		switch {
		case errors.Is(err, sql.ErrNoRows):
			instrument.Active = false
			instrument.TriggerLevel = 7
		case err == nil:
			instrument.Active = dbInstrument.Active
			instrument.TriggerLevel = dbInstrument.TriggerLevel
		default:
			return err
		}

		instrument.MinPrice = minPrice
		instrument.Name = oneFigi.Name
		instrument.Tiker = oneFigi.Ticker
		instrument.Lot = oneFigi.Lot
		instrument.Currency = oneFigi.Currency
		instrument.Exchange = oneFigi.Exchange
		instrument.RealExchange = oneFigi.RealExchange.String()
		instrument.ClassCode = oneFigi.ClassCode
		err = app.repo.WriteInstrument(instrument.getDBObject())
		if err != nil {
			return err
		}
	}

	return nil
}

func (app marketApp) setActive(instrument Instrument, active bool) error {
	instrument.Active = active
	err := app.repo.WriteInstrument(instrument.getDBObject())
	if err != nil {
		return err
	}

	if active {
		err = app.loadPrice(instrument, loadDaysAgo)
	}

	return err
}

func minPrice(share *investapi.Share) int32 {
	minPrice := int32(1)
	if share.MinPriceIncrement != nil && share.MinPriceIncrement.Nano > 0 {
		minPrice = share.MinPriceIncrement.Nano
	}

	return minPrice
}

func (app marketApp) loadPriceParallel(instrument Instrument, days int, waitGroup *sync.WaitGroup) {
	defer waitGroup.Done()
	err := app.loadPrice(instrument, days)
	if err != nil {
		log.Println(err)
	}
}

func (instrument Instrument) String() string {
	return fmt.Sprintf("%s (%s)", instrument.Tiker, instrument.Name)
}

func (app marketApp) loadPrice(instrument Instrument, days int) error {
	if instrument.Figi == "" {
		return fmt.Errorf("%w %s", ErrInstrumentNotFound, instrument)
	}

	daysAgo := timestamppb.New(app.timer.Now().Add(-time.Duration(hoursInDay*days) * time.Hour))

	prices, err := app.tinkoffAPI.GetCandles(instrument.Figi,
		daysAgo,
		timestamppb.Now(),
		investapi.CandleInterval_CANDLE_INTERVAL_DAY)
	if err != nil {
		return err
	}

	for _, candle := range prices {
		candleFilled := Candle{}
		candleFilled.Figi = instrument.Figi
		candleFilled.Tiker = instrument.Tiker
		candleFilled.Time = candle.Time.AsTime().Unix()
		candleFilled.Open = getQuotationValue(candle.Open)
		candleFilled.Close = getQuotationValue(candle.Close)
		candleFilled.Low = getQuotationValue(candle.Low)
		candleFilled.High = getQuotationValue(candle.High)
		candleFilled.Volume = candle.Volume
		err = app.repo.WriteCandle(candleFilled.getDBObject())
		if err != nil {
			return err
		}
	}

	return nil
}

func (app marketApp) writeEvents() {
	events := []Messager{}
	for _, dbEvent := range app.repo.NewEventsBell() {
		event := EventBell{}
		event.readDBObject(dbEvent)
		events = append(events, event)
	}

	bigChanges, err := app.newEventsBigChange()
	if err == nil {
		for _, dbEvent := range bigChanges {
			event := EventBigChange{}
			event.readDBObject(dbEvent)
			events = append(events, event)
		}
	}

	for _, dbEvent := range app.repo.NewEventsBigVolume() {
		event := EventBigVolume{}
		event.readDBObject(dbEvent)
		events = append(events, event)
	}

	for _, event := range events {
		message, err := event.Message()
		if err != nil {
			log.Println(err)

			continue
		}

		err = app.repo.WriteMessage(message.getDBObject())
		if err != nil {
			log.Println(err)
		}
	}
}

func (app marketApp) newEventsBigChange() ([]repository.EventBigChange, error) {
	dayToCheckCandles := 5
	activeInstruments, err := app.repo.GetActiveInstruments()
	if err != nil {
		return nil, err
	}
	activeIDs := make([]string, 0, len(activeInstruments))
	for _, activeInstrument := range activeInstruments {
		activeIDs = append(activeIDs, activeInstrument.Figi)
	}
	instrumentCandles, err := app.repo.ListCandles(repository.ListCandlesFilter{
		From:  app.timer.Now().Add(time.Duration(-dayToCheckCandles) * time.Hour * hoursInDay),
		Figis: optional.From(activeIDs),
	})
	if err != nil {
		return nil, err
	}
	instrumentEvents, err := app.repo.ListEvents(repository.ListEventsFilter{
		From:  app.timer.Now().Add(time.Duration(-dayToCheckCandles) * time.Hour * hoursInDay),
		Figis: optional.From(activeIDs),
	})
	if err != nil {
		return nil, err
	}

	return groupNewEventsBigChange(activeInstruments, instrumentCandles, instrumentEvents)
}

func groupNewEventsBigChange(
	activeInstruments []repository.Instrument,
	instrumentCandles map[string][]repository.Candle,
	instrumentEvents map[string][]repository.Message,
) ([]repository.EventBigChange, error) {
	var result []repository.EventBigChange
out:
	for _, instrument := range activeInstruments {
		candles := instrumentCandles[instrument.Figi]
		if len(candles) == 0 {
			continue
		}

		eventType, valid := eventTypeByCandles(candles, instrument.TriggerLevel)
		if !valid {
			continue
		}
		event := repository.EventBigChange{
			Time:       candles[0].Time,
			Instrument: instrument,
		}
		event.EventType = eventType

		existingEvents, ok := instrumentEvents[instrument.Figi]
		if ok {
			for _, existingEvent := range existingEvents {
				if existingEvent.Time != event.Time {
					continue
				}
				if existingEvent.MsgType == event.EventType {
					continue out
				}
				block := eventsTypeMapBlock[event.EventType]
				for _, blockType := range block {
					if blockType == existingEvent.MsgType {
						continue out
					}
				}
			}
		}
		result = append(result, event)
	}

	return result, nil
}

func eventTypeByCandles(candles []repository.Candle, triggerLevel int8) (int, bool) {
	now := candles[0].Close
	before := candles[0].Open
	if len(candles) > 1 {
		before = candles[1].Close
	}
	change := (now - before) / before

	switch {
	case change > 4*float64(triggerLevel)/100:
		return 2, true
	case change > 2*float64(triggerLevel)/100:
		return 1, true
	case change > 1*float64(triggerLevel)/100:
		return 0, true
	case change < -4*float64(triggerLevel)/100:
		return 5, true
	case change < -2*float64(triggerLevel)/100:
		return 4, true
	case change < -1*float64(triggerLevel)/100:
		return 3, true
	default:
		return 0, false
	}
}

func (app marketApp) getActiveInstruments() ([]Instrument, error) {
	dbInstruments, err := app.repo.GetActiveInstruments()
	if err != nil {
		return nil, err
	}

	instruments := make([]Instrument, len(dbInstruments))
	for index := range dbInstruments {
		instument := Instrument{}
		instument.readDBObject(dbInstruments[index])
		instruments[index] = instument
	}

	return instruments, nil
}

func getQuotationValue(quotation *investapi.Quotation) float64 {
	nano := 1000000000.0

	return float64(quotation.Units) + (float64(quotation.Nano) / nano)
}

func formatDouble(value float64) string {
	multiply := 1.0
	for multiply < 1000000 {
		if multiply*value == math.Round(multiply*value) {
			break
		}
		multiply *= 10
	}
	precision := math.Log10(multiply)
	formatStr := "%." + strconv.Itoa(int(precision)) + "f"

	return fmt.Sprintf(formatStr, value)
}

func (bell Bell) getDBObject() repository.Bell {
	return repository.Bell{
		Figi:      bell.Figi,
		Target:    bell.Target,
		FromBelow: bell.FromBelow,
	}
}

func (instrument Instrument) getDBObject() repository.Instrument {
	return repository.Instrument{
		Figi:         instrument.Figi,
		Name:         instrument.Name,
		Tiker:        instrument.Tiker,
		Currency:     instrument.Currency,
		MinPrice:     instrument.MinPrice,
		Lot:          instrument.Lot,
		TriggerLevel: instrument.TriggerLevel,
		Active:       instrument.Active,
		ClassCode:    sql.NullString{Valid: true, String: instrument.ClassCode},
		Exchange:     sql.NullString{Valid: true, String: instrument.Exchange},
		RealExchange: sql.NullString{Valid: true, String: instrument.RealExchange},
	}
}

func (instrument *Instrument) readDBObject(dbInstrument repository.Instrument) {
	instrument.Figi = dbInstrument.Figi
	instrument.Name = dbInstrument.Name
	instrument.Tiker = dbInstrument.Tiker
	instrument.Currency = dbInstrument.Currency
	instrument.MinPrice = dbInstrument.MinPrice
	instrument.Lot = dbInstrument.Lot
	instrument.TriggerLevel = dbInstrument.TriggerLevel
	instrument.Active = dbInstrument.Active
	instrument.ClassCode = dbInstrument.ClassCode.String
	instrument.Exchange = dbInstrument.Exchange.String
	instrument.RealExchange = dbInstrument.RealExchange.String
}

func (message Message) getDBObject() repository.Message {
	return repository.Message{
		Figi:    message.Figi,
		Message: message.Message,
		Time:    message.Time,
		MsgType: int(message.MsgType),
	}
}

func (candle Candle) getDBObject() repository.Candle {
	return repository.Candle{
		Figi:   candle.Figi,
		Time:   candle.Time,
		Open:   candle.Open,
		Close:  candle.Close,
		Low:    candle.Low,
		High:   candle.High,
		Volume: candle.Volume,
	}
}
