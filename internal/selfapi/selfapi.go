package selfapi

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/ffSaschaGff/tinkoff-invest-watcher/internal/marketdata"
)

// APIError - error message for API.
type APIError struct {
	Message string `json:"message"`
}

// EventForBot - event for bot.
type EventForBot struct {
	Type    string
	Message string
}

type BellSetter struct {
	Tiker  string  `json:"tiker"`
	Target float64 `json:"target"`
}

type InstrumentPut struct {
	Tiker        string `json:"tiker"`
	TriggerLevel int8   `json:"triggerLevel"`
	Active       bool   `json:"active"`
	HasTrigger   bool
	HasActive    bool
}

type APIHandler interface {
	HandlerInstrumentsPut(context *gin.Context)
	HandlerEventsGet(context *gin.Context)
	HandlerBellsPost(context *gin.Context)
	HandlerInstrumentsGet(context *gin.Context)
	HandlerForceUpdatePost(context *gin.Context)
}

type apiHandler struct {
	app marketdata.MarketApp
}

const (
	TypeMessage = "message"
)

var ErrTikerIsRequired = errors.New("tiker is required")

func NewHandler(app marketdata.MarketApp) APIHandler {
	return apiHandler{
		app: app,
	}
}

// StartApiServer - set handlers and stert api.
func StartAPIServer(handler APIHandler) {
	router := gin.Default()
	addRoutes(router, handler)
	err := router.Run()
	if err != nil {
		log.Fatalln(err.Error())
	}
}

func addRoutes(engine *gin.Engine, handler APIHandler) {
	engine.PUT("/api/latest/instruments", handler.HandlerInstrumentsPut)
	engine.GET("/api/latest/events", handler.HandlerEventsGet)
	engine.POST("/api/latest/bells", handler.HandlerBellsPost)
	engine.GET("/api/latest/instruments/:tiker", handler.HandlerInstrumentsGet)
	engine.POST("/api/latest/force_update", handler.HandlerForceUpdatePost)
}

func (handler apiHandler) HandlerForceUpdatePost(context *gin.Context) {
	err := handler.app.ForceUpdate()
	if err != nil {
		context.String(http.StatusBadRequest, apiErrByErr(err).getBody())

		return
	}
	context.JSON(http.StatusOK, "{}")
}

func (handler apiHandler) HandlerEventsGet(context *gin.Context) {
	eventsResponse := []EventForBot{}
	events, err := handler.app.GetNewEvents()
	if err != nil {
		context.String(http.StatusBadRequest, apiErrByErr(err).getBody())

		return
	}
	for _, event := range events {
		eventResponse := EventForBot{}
		eventResponse.Message = event
		eventResponse.Type = TypeMessage
		eventsResponse = append(eventsResponse, eventResponse)
	}
	context.JSON(http.StatusOK, eventsResponse)
}

func (handler apiHandler) HandlerInstrumentsGet(context *gin.Context) {
	tiker := context.Param("tiker")
	err := handler.app.SendTikerInfo(tiker)
	if err != nil {
		context.String(http.StatusBadRequest, apiErrByErr(err).getBody())

		return
	}
	context.JSON(http.StatusOK, "{}")
}

func (handler apiHandler) HandlerInstrumentsPut(context *gin.Context) {
	instrument := InstrumentPut{}
	body, err := io.ReadAll(context.Request.Body)
	if err != nil {
		context.String(http.StatusBadRequest, apiErrByErr(err).getBody())

		return
	}
	err = json.Unmarshal(body, &instrument)
	if err != nil {
		context.String(http.StatusBadRequest, apiErrByErr(err).getBody())

		return
	}

	err = handler.updateInstrumentPut(instrument)
	if err != nil {
		context.String(http.StatusBadRequest, apiErrByErr(err).getBody())

		return
	}
	context.String(http.StatusOK, "{}")
}

func (handler apiHandler) HandlerBellsPost(context *gin.Context) {
	bell := BellSetter{}
	body, err := io.ReadAll(context.Request.Body)
	if err != nil {
		context.String(http.StatusBadRequest, apiErrByErr(err).getBody())

		return
	}
	err = json.Unmarshal(body, &bell)
	if err != nil {
		context.String(http.StatusBadRequest, apiErrByErr(err).getBody())

		return
	}
	instrument := marketdata.Instrument{}
	instrument.Tiker = bell.Tiker
	err = handler.app.SetBell(&instrument, bell.Target)
	if err != nil {
		context.String(http.StatusBadRequest, apiErrByErr(err).getBody())

		return
	}
	context.String(http.StatusOK, "{}")
}

// UpdateInstrumentPut - update existed instrument.
func (handler apiHandler) updateInstrumentPut(apiInstrument InstrumentPut) error {
	if apiInstrument.Tiker == "" {
		return ErrTikerIsRequired
	}

	if !handler.app.TikerExist(apiInstrument.Tiker) {
		return marketdata.ErrInstrumentNotFound
	}

	errors := []error{}

	if apiInstrument.HasActive {
		err := handler.app.SetActiveByTiker(apiInstrument.Tiker, apiInstrument.Active)
		if err != nil {
			errors = append(errors, err)
		}
	}

	if apiInstrument.HasTrigger {
		err := handler.app.SetLevelByTiker(apiInstrument.Tiker, apiInstrument.TriggerLevel)
		if err != nil {
			errors = append(errors, err)
		}
	}

	if len(errors) > 0 {
		return errors[0]
	}

	if apiInstrument.HasActive {
		message := fmt.Sprintf("Инструменту %s установлена активность %t", apiInstrument.Tiker, apiInstrument.Active)
		err := handler.app.SendMessage(message)
		if err != nil {
			return err
		}
	}

	if apiInstrument.HasTrigger {
		message := fmt.Sprintf("Инструменту %s установлен уровень %d", apiInstrument.Tiker, apiInstrument.TriggerLevel)
		err := handler.app.SendMessage(message)
		if err != nil {
			return err
		}
	}

	return nil
}

func (apiError APIError) getBody() string {
	bytes, err := json.Marshal(&apiError)
	if err != nil {
		return `{"message": "server error"}`
	}

	return string(bytes)
}

func apiErrByErr(err error) APIError {
	apiErr := APIError{}
	apiErr.Message = err.Error()

	return apiErr
}
