package selfapi

import (
	"encoding/json"
	"errors"
	"fmt"
)

var (
	ErrLostRequiredField = errors.New("lost required field")
	ErrWrongFieldFormat  = errors.New("wrong field format")
)

// For instrument activity or trigget level can be empty. In that case we take them from DB.
func (instrument *InstrumentPut) UnmarshalJSON(data []byte) error {
	rawResult := make(map[string]interface{})
	err := json.Unmarshal(data, &rawResult)
	if err != nil {
		return err
	}

	if _, exist := rawResult["tiker"]; !exist {
		return fmt.Errorf("%w: %s", ErrLostRequiredField, "tiker")
	}

	var success bool
	instrument.Tiker, success = rawResult["tiker"].(string)
	if !success {
		return fmt.Errorf("%w: %s", ErrWrongFieldFormat, "tiker")
	}

	_, exist := rawResult["active"]
	instrument.HasActive = exist
	if exist {
		instrument.Active, success = rawResult["active"].(bool)
		if !success {
			return fmt.Errorf("%w: %s", ErrWrongFieldFormat, "active")
		}
	}

	_, exist = rawResult["trigger_level"]
	instrument.HasTrigger = exist
	if exist {
		newLevel, success := rawResult["trigger_level"].(float64)
		if !success {
			return fmt.Errorf("%w: %s", ErrWrongFieldFormat, "trigger_level")
		}
		instrument.TriggerLevel = int8(newLevel)
	}

	return nil
}
