package repository

import (
	"database/sql"
	"log"
	"time"

	"github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
	"gitlab.com/ffSaschaGff/tinkoff-invest-watcher/internal/optional"
)

// MarketRepo - repo postgres realization.
type PostgresMarketRepo struct {
	database *sqlx.DB
}

// Instrument - type for day instrument in DB.
type Instrument struct {
	Figi         string `db:"id"`
	Name         string `db:"title"`
	Tiker        string
	Currency     string
	ClassCode    sql.NullString `db:"class_code"`
	Exchange     sql.NullString
	RealExchange sql.NullString `db:"real_exchange"`
	MinPrice     int32          `db:"min_price"`
	Lot          int32
	TriggerLevel int8 `db:"tigger_level"`
	Active       bool
}

// Candle - type for day candle in DB.
type Candle struct {
	Figi   string `db:"id"`
	Time   int64  `db:"ts"`
	Open   float64
	Close  float64
	Low    float64
	High   float64
	Volume int64
}

type ListCandlesFilter struct {
	From  time.Time
	Figis optional.Optional[[]string]
}

type ListEventsFilter struct {
	From  time.Time
	Figis optional.Optional[[]string]
}

// Bell - type for bell in DB.
type Bell struct {
	Figi      string
	Target    float64
	FromBelow bool
}

// Message - type for message in DB.
type Message struct {
	Figi    string
	Message string
	Time    int64 `db:"candle_ts"`
	MsgType int   `db:"type"`
}

type EventBigChange struct {
	Time       int64
	EventType  int
	Instrument Instrument
}

type EventBigVolume struct {
	Time       int64
	Instrument Instrument
}

type EventBell struct {
	Time       int64
	Price      float64
	Instrument Instrument
}

// NewMarketRepo - PostgresMarketRepo constructor.
func NewMarketRepo(database *sqlx.DB) *PostgresMarketRepo {
	return &PostgresMarketRepo{
		database: database,
	}
}

func (repo *PostgresMarketRepo) ListEvents(filter ListEventsFilter) (map[string][]Message, error) {
	queryBuilder := squirrel.Select(
		"figi",
		"candle_ts",
		"type",
		"message",
	).From("events").
		Where(squirrel.GtOrEq{"candle_ts": filter.From.Unix()}).
		PlaceholderFormat(squirrel.Dollar)
	if len(filter.Figis.Value) > 0 {
		queryBuilder = queryBuilder.Where(squirrel.Eq{"figi": filter.Figis.Value})
	}

	query, args, err := queryBuilder.ToSql()
	if err != nil {
		return nil, err
	}

	result := map[string][]Message{}
	rows, err := repo.database.Queryx(query, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var msg Message
		err = rows.StructScan(&msg)
		if err != nil {
			return nil, err
		}
		group := result[msg.Figi]
		group = append(group, msg)
		result[msg.Figi] = group
	}

	return result, nil
}

// ListCandles - ask for candles, return grouped by figi
func (repo *PostgresMarketRepo) ListCandles(filter ListCandlesFilter) (map[string][]Candle, error) {
	queryBuilder := squirrel.Select(
		"id",
		"ts",
		"open",
		"close",
		"low",
		"high",
		"volume",
	).From("candles").
		Where(squirrel.GtOrEq{"ts": filter.From.Unix()}).
		OrderBy("ts desc").
		PlaceholderFormat(squirrel.Dollar)

	if filter.Figis.Valid {
		queryBuilder = queryBuilder.Where(squirrel.Eq{"id": filter.Figis.Value})
	}

	query, args, err := queryBuilder.ToSql()
	if err != nil {
		return nil, err
	}

	result := map[string][]Candle{}
	rows, err := repo.database.Queryx(query, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var candle Candle
		err = rows.StructScan(&candle)
		if err != nil {
			return nil, err
		}
		group := result[candle.Figi]
		group = append(group, candle)
		result[candle.Figi] = group
	}

	return result, nil
}

// ClearGarbage - delete all old data.
func (repo *PostgresMarketRepo) ClearGarbage() {
	hoursToClear := 2160 // 90 days
	timeLimit := time.Now().Add(time.Duration(-1*hoursToClear) * time.Hour).Unix()
	queryBuilder := squirrel.Delete("candles").Where(squirrel.Lt{"ts": timeLimit}).PlaceholderFormat(squirrel.Dollar)
	query, args, err := queryBuilder.ToSql()
	if err != nil {
		log.Println(err.Error())
	}
	_, err = repo.database.Exec(query, args...)
	if err != nil {
		log.Println(err.Error())
	}
}

func (repo *PostgresMarketRepo) GetActiveInstruments() ([]Instrument, error) {
	queryBuilder := squirrel.Select(
		"id",
		"tiker",
		"title",
		"min_price",
		"active",
		"lot",
		"currency",
		"tigger_level",
		"exchange",
		"real_exchange",
		"class_code",
	).From("figi").Where(squirrel.Eq{"active": true}).PlaceholderFormat(squirrel.Dollar)
	query, args, err := queryBuilder.ToSql()
	if err != nil {
		return nil, err
	}
	rows, err := repo.database.Queryx(query, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	figi := []Instrument{}
	for rows.Next() {
		instument := Instrument{}
		err = rows.StructScan(&instument)
		if err != nil {
			return nil, err
		}
		figi = append(figi, instument)
	}

	return figi, nil
}

// UpdateAggregates - update aggregates of figi i.e. avg volume.
func (repo *PostgresMarketRepo) UpdateAggregates() {
	_, err := repo.database.Exec(updateAggregatesQuerry())
	if err != nil {
		log.Println(err)
	}
}

// ReadInstrument - read instrument from DB.
func (repo *PostgresMarketRepo) ReadInstrument(instrument Instrument) (Instrument, error) {
	queryBuilder := squirrel.Select(
		"id",
		"tiker",
		"title",
		"min_price",
		"active",
		"lot",
		"currency",
		"tigger_level",
		"exchange",
		"real_exchange",
		"class_code",
	).From("figi").PlaceholderFormat(squirrel.Dollar)
	if instrument.Figi == "" {
		queryBuilder = queryBuilder.Where(squirrel.Eq{"tiker": instrument.Tiker})
	} else {
		queryBuilder = queryBuilder.Where(squirrel.Eq{"id": instrument.Figi})
	}
	query, args, err := queryBuilder.ToSql()
	if err != nil {
		return Instrument{}, err
	}

	err = repo.database.QueryRowx(query, args...).StructScan(&instrument)
	if err != nil {
		return Instrument{}, err
	}

	return instrument, nil
}

func (repo *PostgresMarketRepo) WriteInstrument(instrument Instrument) error {
	queryBuilder := squirrel.Insert("figi").SetMap(
		map[string]interface{}{
			"id":            instrument.Figi,
			"tiker":         instrument.Tiker,
			"title":         instrument.Name,
			"min_price":     instrument.MinPrice,
			"active":        instrument.Active,
			"lot":           instrument.Lot,
			"currency":      instrument.Currency,
			"tigger_level":  instrument.TriggerLevel,
			"exchange":      instrument.Exchange,
			"real_exchange": instrument.RealExchange,
			"class_code":    instrument.ClassCode,
		},
	).Suffix(`ON CONFLICT (id) DO UPDATE
	SET tiker = excluded.tiker,
	title = excluded.title,
	min_price = excluded.min_price,
	active = excluded.active,
	lot = excluded.lot,
	currency = excluded.currency,
	tigger_level = excluded.tigger_level,
	exchange = excluded.exchange,
	real_exchange = excluded.real_exchange,
	class_code = excluded.class_code`).PlaceholderFormat(squirrel.Dollar)
	query, args, err := queryBuilder.ToSql()
	if err != nil {
		return err
	}

	_, err = repo.database.Exec(query, args...)

	return err
}

func (repo *PostgresMarketRepo) WriteCandle(candle Candle) error {
	queryBuilder := squirrel.Insert("candles").SetMap(
		map[string]interface{}{
			"id":     candle.Figi,
			"ts":     candle.Time,
			"open":   candle.Open,
			"close":  candle.Close,
			"low":    candle.Low,
			"high":   candle.High,
			"volume": candle.Volume,
		},
	).Suffix(`ON CONFLICT (id, ts) DO UPDATE
	SET open = excluded.open,
	close = excluded.close,
	low = excluded.low,
	high = excluded.high,
	volume = excluded.volume`).PlaceholderFormat(squirrel.Dollar)
	query, args, err := queryBuilder.ToSql()
	if err != nil {
		return err
	}

	_, err = repo.database.Exec(query, args...)

	return err
}

func (repo *PostgresMarketRepo) WriteBell(bell Bell) error {
	queryBuilder := squirrel.Insert("bells").SetMap(
		map[string]interface{}{
			"figi":       bell.Figi,
			"value":      bell.Target,
			"from_below": bell.FromBelow,
		},
	).Suffix(`ON CONFLICT(figi,value) DO NOTHING`).PlaceholderFormat(squirrel.Dollar)
	query, args, err := queryBuilder.ToSql()
	if err != nil {
		return err
	}

	_, err = repo.database.Exec(query, args...)

	return err
}

func (repo *PostgresMarketRepo) WriteMessage(message Message) error {
	queryBuilder := squirrel.Insert("events").SetMap(
		map[string]interface{}{
			"figi":      message.Figi,
			"candle_ts": message.Time,
			"sended":    false,
			"type":      message.MsgType,
			"message":   message.Message,
		},
	).PlaceholderFormat(squirrel.Dollar)
	query, args, err := queryBuilder.ToSql()
	if err != nil {
		return err
	}
	_, err = repo.database.Exec(query, args...)

	return err
}

func (repo *PostgresMarketRepo) GetMessages() ([]string, error) {
	hoursInDay := 24
	queryBuilder := squirrel.Select(
		"message",
		"id",
	).From("events").Where(
		squirrel.And{
			squirrel.Eq{"sended": false},
			squirrel.Gt{"candle_ts": time.Now().Truncate(time.Duration(hoursInDay) * time.Hour).Unix()},
			squirrel.NotEq{"message": nil},
		},
	).PlaceholderFormat(squirrel.Dollar)
	query, args, err := queryBuilder.ToSql()
	if err != nil {
		return nil, err
	}
	rows, err := repo.database.Queryx(query, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	messages := []string{}
	ids := make([]interface{}, 0)
	for rows.Next() {
		var message string
		var idMsg int
		err = rows.Scan(&message, &idMsg)
		if err != nil {
			return nil, err
		}
		messages = append(messages, message)
		ids = append(ids, idMsg)
	}
	if len(messages) == 0 {
		return messages, nil
	}

	qbSetSended := squirrel.Update("events").
		Set("sended", true).
		Where(squirrel.Eq{"id": ids}).
		PlaceholderFormat(squirrel.Dollar)
	query, args, err = qbSetSended.ToSql()
	if err != nil {
		return nil, err
	}
	_, err = repo.database.Exec(query, args...)
	if err != nil {
		return nil, err
	}

	return messages, nil
}

func (repo *PostgresMarketRepo) GetBells(figi string) ([]float64, error) {
	queryBuilder := squirrel.Select("value").
		From("bells").
		Where(squirrel.Eq{"figi": figi}).
		PlaceholderFormat(squirrel.Dollar)
	query, args, err := queryBuilder.ToSql()
	if err != nil {
		return nil, err
	}

	bells := []float64{}
	rows, err := repo.database.Queryx(query, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		bell := 0.0
		err = rows.Scan(&bell)
		if err != nil {
			return nil, err
		}
		bells = append(bells, bell)
	}

	return bells, nil
}

func (repo *PostgresMarketRepo) NewEventsBigChange() []EventBigChange {
	result := []EventBigChange{}
	hoursGapMatter := 120
	hoursInDay := 24
	today := time.Now().Truncate(time.Duration(hoursInDay) * time.Hour).Unix()
	timeLimit := time.Now().Add(-time.Duration(hoursGapMatter) * time.Hour).Unix()
	rows, err := repo.database.Queryx(newEventsBigChangeQuerry(), timeLimit, today)
	if err != nil {
		log.Println(err)

		return result
	}
	defer rows.Close()

	for rows.Next() {
		message := EventBigChange{}
		figi := ""
		err = rows.Scan(&figi, &message.Time, &message.EventType)
		if err != nil {
			log.Println(err)
		}
		message.Instrument.Figi = figi
		message.Instrument, err = repo.ReadInstrument(message.Instrument)
		if err != nil {
			log.Println(err)
		}
		result = append(result, message)
	}

	return result
}

func (repo *PostgresMarketRepo) NewEventsBigVolume() []EventBigVolume {
	result := []EventBigVolume{}
	rows, err := repo.database.Queryx(newEventsBigVolumeQuerry())
	if err != nil {
		log.Println(err)

		return result
	}
	defer rows.Close()

	for rows.Next() {
		message := EventBigVolume{}
		figi := ""
		err = rows.Scan(&figi, &message.Time)
		if err != nil {
			log.Println(err)
		}
		message.Instrument.Figi = figi
		message.Instrument, err = repo.ReadInstrument(message.Instrument)
		if err != nil {
			log.Println(err)
		}
		result = append(result, message)
	}

	return result
}

func (repo *PostgresMarketRepo) NewEventsBell() []EventBell {
	result := []EventBell{}
	rows, err := repo.database.Queryx(newEventsBellQuerry())
	if err != nil {
		log.Println(err)

		return result
	}
	defer rows.Close()

	for rows.Next() {
		message := EventBell{}
		figi := ""
		err = rows.Scan(&figi, &message.Time, &message.Price)
		if err != nil {
			log.Println(err)
		}
		message.Instrument.Figi = figi
		message.Instrument, err = repo.ReadInstrument(message.Instrument)
		if err != nil {
			log.Println(err)
		}
		result = append(result, message)
	}

	return result
}
